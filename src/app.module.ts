import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CountriesController } from './countries/countries.controller';
import { Country } from './countries/countries.model';
import { CountriesModule } from './countries/countries.module';
import { CountriesService } from './countries/countries.service';
import { PaginateModule } from 'nestjs-sequelize-paginate';

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: 'sql4.freemysqlhosting.net',
      port: 3306,
      username: 'sql4451001',
      password: 'HNJQhrTpz4',
      database: 'sql4451001',
      models: [Country],
      autoLoadModels: true,
      synchronize: true,
    }),
    CountriesModule,
    PaginateModule.forRoot({
      url: 'http://localhost:3000',
    }),
  ],
  controllers: [AppController, CountriesController],
  providers: [AppService, CountriesService],
})
export class AppModule {}
