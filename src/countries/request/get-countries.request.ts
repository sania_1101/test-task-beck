export class GetCountriesRequest {
  limit: string;
  offset: string;
  searchTerm: string;
  orderBy: string;
  orderType: string;
  where: string;
}
