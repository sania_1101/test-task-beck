import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { CountriesController } from './countries.controller';
import { CountriesService } from './countries.service';
import { Country } from './countries.model';

@Module({
  imports: [SequelizeModule.forFeature([Country])],
  providers: [CountriesService],
  controllers: [CountriesController],
  exports: [SequelizeModule],
})
export class CountriesModule {}
