import { ApiProperty } from '@nestjs/swagger';
import { Column, Table, Model } from 'sequelize-typescript';

@Table
export class Country extends Model {
  @Column
  @ApiProperty()
  name: string;

  @Column
  @ApiProperty()
  code: string;

  @Column
  @ApiProperty()
  flag: string;

  @Column
  @ApiProperty()
  description: string;

  @Column
  @ApiProperty()
  lat: string;

  @Column
  @ApiProperty()
  long: string;
}
