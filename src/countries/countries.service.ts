import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

import { Country } from './countries.model';
import { GetCountriesRequest } from './request/get-countries.request';
import { CountriesListResponse } from './response/country-list.response';

@Injectable()
export class CountriesService {
  constructor(
    @InjectModel(Country)
    private countryModel: typeof Country,
  ) {}
  async findAll(params: GetCountriesRequest): Promise<CountriesListResponse> {
    const where = params.where ? JSON.parse(params.where) : [];
    const whereFiler = where
      .filter((it) => it.type !== 'search')
      .map((item) => item.name + item.operation + '"' + item.value + '"')
      .join(' AND ');

    const whereSearch = where
      .filter((it) => {
        return it.type === 'search';
      })
      .map((item) => {
        return `${item.name} ${item.operation} "${item.value}"`;
      })
      .join(' OR ');

    let whereCombination = '';

    if (whereSearch.length && whereFiler.length) {
      whereCombination = `(${whereSearch}) AND (${whereFiler})`;
    } else if (whereSearch.length || whereFiler.length) {
      whereCombination = `${whereSearch} ${whereFiler}`;
    }
    const whereStr =
      whereCombination.length > 10 ? `WHERE ${whereCombination}` : '';

    const data = (
      await this.countryModel.sequelize.query(
        `SELECT * FROM Countries  ${whereStr}
         ORDER BY ${params.orderBy} ${params.orderType} limit ${params.offset}, ${params.limit};`,
      )
    )[0] as unknown as Country[];

    const count = await this.countryModel.sequelize.query(
      `SELECT COUNT(*) FROM Countries  ${whereStr} ;`,
    );

    return { data, count: count[0][0]['COUNT(*)'] };
  }

  async findOne(id: string): Promise<Country> {
    return this.countryModel.findOne({
      where: {
        id,
      },
    });
  }

  async remove(id: string): Promise<void> {
    const user = await this.findOne(id);
    await user.destroy();
  }

  async create(country: Country): Promise<Country> {
    return this.countryModel.create(country);
  }

  async update(id: any, country: Country): Promise<any> {
    await this.countryModel.update(
      { ...country },
      { where: { id }, returning: true },
    );

    return;
  }
}
