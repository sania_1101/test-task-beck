import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiBody, ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Country } from './countries.model';
import { CountriesService } from './countries.service';
import { GetCountriesRequest } from './request/get-countries.request';
import { CountriesListResponse } from './response/country-list.response';

@ApiTags('countries')
@Controller('countries')
export class CountriesController {
  constructor(private countriesService: CountriesService) {}

  @Get()
  @ApiQuery({ type: GetCountriesRequest })
  async findAll(
    @Query() params: GetCountriesRequest,
  ): Promise<CountriesListResponse> {
    return await this.countriesService.findAll(params);
  }

  @Get(':id')
  @ApiParam({ name: 'id' })
  async findById(@Param() params): Promise<Country> {
    return await this.countriesService.findOne(params.id);
  }

  @Post()
  async create(@Body() country: Country): Promise<Country> {
    return await this.countriesService.create(country);
  }

  @Put(':id')
  @ApiParam({ name: 'id' })
  @ApiBody({ type: Country })
  async update(@Param() params, @Body() country): Promise<Country> {
    await this.countriesService.update(params.id, country);
    return null;
  }

  @Delete(':id')
  @ApiParam({ name: 'id' })
  async delete(@Param() params): Promise<null> {
    await this.countriesService.remove(params.id);
    return null;
  }
}
