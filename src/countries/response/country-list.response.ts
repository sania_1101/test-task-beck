import { Country } from '../countries.model';

export interface CountriesListResponse {
  data: Country[];
  count: number;
}
